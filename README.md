# bingus-api

A silly little program made to serve a new picture of bingus everyday.

# Installation

 - Change the ``BaseURL`` constant to your hosting URL.

 - (Optional) Change photos the API serves (located in ``images/``)

 - Run ``go build`` to get the ``bingus-api`` binary.

 - Run ``bingus-api``

 - ??

 - Profit! 

# Todos

 While this project is a joke, it could be made better if we read some of the variables (like ``BaseURL``) at runtime. Feel free to make any pull requests for features. They might just get merged!
