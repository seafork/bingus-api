package main

import (
	"embed"
	"encoding/json"
	"fmt"
	"io/fs"
	"log"
	"math/rand"
	"net/http"
	"time"
)

//go:embed images/*
var content embed.FS

const Port = "9000"
const APIVersion = "1.0"
const WaitInHours = 1
const BaseURL = "http://localhost:" + Port + "/"

type App struct {
	Date         time.Time
	CurrentImage string
}

func updateCurrentImage(a *App) {
	images, err := content.ReadDir("images")
	if err != nil {
		log.Fatal("Couldn't read embeded FS! Err: " + err.Error())
	}

	imageMap := make(map[int]string)
	for i, image := range images {
		imageMap[i] = image.Name()
	}

	for {
		random := rand.Intn(len(imageMap))
		if imageMap[random] != a.CurrentImage {
			a.CurrentImage = imageMap[random]
			break
		}
	}
}

func updateApp(a *App) {
	for {
		day := time.Now()

		dayHasPassed := day.Day() > a.Date.Day()
		newMonth := day.Month().String() != a.Date.Month().String()

		if dayHasPassed || newMonth {
			updateCurrentImage(a)
		}

		a.Date = day
		time.Sleep(time.Hour * WaitInHours)
	}
}

func (a *App) return_json(w http.ResponseWriter, r *http.Request) {
	jsonB := make(map[string]string)
	jsonB["date"] = a.Date.UTC().String()
	jsonB["picture"] = BaseURL + "images/" + a.CurrentImage
	jsonB["api-version"] = APIVersion
	jsonF, error := json.Marshal(jsonB)

	if error != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Error constructing json!"))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonF)
	return
}

func main() {
	fmt.Println("Running Bingus API version "+APIVersion,
		"\nSpinning up server at:"+BaseURL)

	rand.Seed(time.Now().UnixNano())

	app := App{
		Date:         time.Now(),
		CurrentImage: "",
	}
	updateCurrentImage(&app)
	go updateApp(&app)

	imageFS, _ := fs.Sub(content, "images")
	http.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.FS(imageFS))))

	http.HandleFunc("/", app.return_json)
	log.Fatal(http.ListenAndServe(":"+Port, nil))
}
